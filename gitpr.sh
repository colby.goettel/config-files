#!/bin/bash

has_modified=$(git ls-files --modified)
GITROOT=$(git rev-parse --show-cdup)

if [[ -n $GITROOT ]] ; then
    cd "$GITROOT"
fi

git fetch

if [[ -n $has_modified ]] ; then
    git stash save "git_pull_rebase-$(date +%Y%m%d%H%M)"
fi

git pull --rebase --stat --ff-only

if [[ -n $has_modified ]] ; then
    git stash pop
fi
