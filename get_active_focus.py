# This Python script runs on Mac and prints which window has the current window focus. It's useful if something is stealing your window focus and you need to track it down.
# See: https://apple.stackexchange.com/q/123730

# Usage: sudo python3 get_active_focus.py
# Dependencies: xcode developer tools

from AppKit import NSWorkspace
import time
workspace = NSWorkspace.sharedWorkspace()
active_app = workspace.activeApplication()['NSApplicationName']
print('Active focus: ' + active_app)
while True:
    time.sleep(1)
    prev_app = active_app
    active_app = workspace.activeApplication()['NSApplicationName']
    if prev_app != active_app:
        print('Focus changed to: ' + active_app)
